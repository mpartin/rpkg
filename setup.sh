#!/bin/sh

BS_LOG=/tmp/rpkg_bootstrap.log
RUBY_URI=ftp://ftp.ruby-lang.org/pub/ruby/1.9/ruby-1.9.3-p448.tar.gz
CONFIGURE_ARGS="--prefix=${HOME}/.rpkg/.bootstrap --disable-install-doc"
START_DIR=${PWD}

if [ ! -d ${HOME}/.rpkg/.bootstrap ]; then
  mkdir -p ${HOME}/.rpkg/.bootstrap
fi

printf '\nBootstrapping ruby for Rpkg:\nDown ${HOME} package management.\n\n'

cd /tmp
printf "Fetching $(basename ${RUBY_URI})..."
if [ ! -z "$(which wget)" ]; then
  $(which wget) -qO /tmp/ruby.tgz ${RUBY_URI}
elif [ ! -z "$(which curl)" ]; then
  $(which curl) -q ${RUBY_URI} > /tmp/ruby.tgz
fi
echo " done"

printf "Extracting..."
tar zxf ruby.tgz && echo " done"
cd ruby-1.9.3-p448

printf "Configuring..."
./configure ${CONFIGURE_ARGS} >>${BS_LOG} 2>&1 && echo " done"

printf "Compiling..."
make -j4 >>${BS_LOG} 2>&1 && echo " done"

printf "Installing..."
make install >>${BS_LOG} 2>&1 && echo " done"

printf "Cleaning up..."
cd /tmp
rm -rf ruby.tgz ruby-1.9.3-p448
echo " done"

printf "Installing dependancies..."
PATH=${HOME}/.rpkg/.bootstrap/bin:${PATH}
GEM_INSTALL="gem install --no-ri --no-rdoc"
${GEM_INSTALL} thor sqlite3 libarchive >>${BS_LOG} 2>&1 && echo " done"

printf "Installing Rpkg..."
cd ${START_DIR}
for i in `ls`; do
  if [ -d ${i} ]; then
    # copy over the directory, preserving all permissions and attributes
    tar_src="tar -f- -c ${i}"
    tar_dst="tar -f- -C ${HOME}/.rpkg/ -x"
    ${tar_src} 2>/dev/null|${tar_dst} 2>/dev/null
  fi
done
echo " done"
