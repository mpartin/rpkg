#!/usr/bin/env ruby
################################################################################
#
# Copyright 2013 Mike 'Fuzzy'  Partin   <fuzzy@fu-manchu.org>
# Copyright 2013 Matt 'Roider' Schiros  <schirs@invisihosting.com>
#
################################################################################
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  
################################################################################

## Path hack
$LOAD_PATH << "#{File.basename(__FILE__)}/../lib/"

##-----------------
## Module imports

# Stdlib
#---------
require 'yaml'
require 'curses'

# 3rd party
#------------
require 'thor'
require 'sqlite3'

# Internal
#-----------
require 'rpkg'

##----------
## Globals
$rpkg = {
  :dirs     => {
    :base   => nil,
    :pd     => nil,
    :pkg    => nil
  },
  :session  => {
    :id     => nil,
    :dir    => nil
  }
}

##---------------
## Config logic

# Configfiles
#--------------

["#{ENV['HOME']}/.rpkg/etc/rpkg.yml",
 "#{ENV['HOME']}/.rpkgrc"].each { |cfg|
  if File.exist?(cfg)
    tmp = YAML.load(File.read(cfg))
    tmp.keys.each {|key|
      # Lets get our types right
      if tmp[key].class == Hash
        $rpkg[key.to_sym] = Hash.new
        tmp[key].keys.each {|skey|
          $rpkg[key.to_sym][skey.to_sym] = tmp[key][skey]
        }
      end
    }
  end
}

# Environment
#--------------
if ENV.include?('RPKG_BASE_DIR')
  $rpkg[:dirs][:base]   = ENV['RPKG_BASE_DIR']
else
  puts 'Operation cannot continue without proper configuration.'
  exit
end
# TMP_DIR
if ENV.include?('RPKG_TMP_DIR')
  $rpkg[:dirs][:temp]     = ENV['RPKG_TMP_DIR']
elsif $rpkg[:dirs][:base]
  $rpkg[:dirs][:temp]     = "#{$rpkg[:dirs][:base]}/tmp"
end
# PANTRY
if ENV.include?('RPKG_PANTRY')
  $rpkg[:dirs][:pantry]   = ENV['RPKG_PANTRY']
elsif $rpkg[:dirs][:base]
  $rpkg[:dirs][:pantry]   = "#{$rpkg[:dirs][:base]}/pantry"
end
# PD_DIR
if ENV.include?('RPKG_PD_DIR')
  $rpkg[:dirs][:pd]       = ENV['RPKG_PD_DIR']
elsif $rpkg[:dirs][:base]
  $rpkg[:dirs][:pd]       = "#{$rpkg[:dirs][:base]}/etc/pkgdefs"
end
# PKG_DIR
if ENV.include?('RPKG_PKG_DIR')
  $rpkg[:dirs][:pkg]      = ENV['RPKG_PKG_DIR']
elsif $rpkg[:dirs][:base]
  $rpkg[:dirs][:pkg]      = "#{$rpkg[:dirs][:base]}/depot"
end
#-- Session
# ID
if ENV.include?('RPKG_SESSION_ID')
  $rpkg[:session][:id]    = ENV['RPKG_SESSION_ID']
  # SESSION_DIR
  if ENV.include?('RPKG_SESSION_DIR')
    $rpkg[:session][:dir] = ENV['RPKG_SESSION_DIR']
  else
    tmp = "#{$rpkg[:dirs][:base]}/sessions/#{$rpkg[:session][:id]}"
    $rpkg[:session][:dir] = tmp
  end
else
  $rpkg[:session][:id]    = RPkg::Utils.genSessionID
  $rpkg[:session][:dir]   = "#{$rpkg[:dirs][:base]}/sessions/#{$rpkg[:session][:id]}"
end

Curses.init_screen
$rpkg[:columns] = Curses.cols
Curses.close_screen

##-------------
## Main logic

$UI = RPkg::UI.new
#RPkg::Dsl.new('./test.pd')
RPkg::CommandLine::Processor.start(ARGV)

