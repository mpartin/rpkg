################################################################################
#
# Copyright 2013 Mike 'Fuzzy' Partin <fuzzy@fu-manchu.org>
#
################################################################################
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  
################################################################################

###################
## Module imports

## Stdlib

## 3rd party
  
class String
  # Normal colors
  def black;       colorize(self, "[0m[30");     end
  def red;         colorize(self, "[0m[31");     end
  def green;       colorize(self, "[0m[32");     end
  def yellow;      colorize(self, "[0m[33");     end
  def blue;        colorize(self, "[0m[34");     end
  def purple;      colorize(self, "[0m[35");     end
  def cyan;        colorize(self, "[0m[36");     end
  def white;       colorize(self, "[0m[37");     end
  
  # Fun stuff
  def clean;       colorize(self, "[0");           end
  def bold;        colorize(self, "[1");           end
  def underline;   colorize(self, "[4");           end
  def blink;       colorize(self, "[5");           end
  def reverse;     colorize(self, "[7");           end
  def conceal;     colorize(self, "[8");           end
  
  # Blanking
  def clear_scr;   colorize(self, "[2", mode="J"); end
  
  # Placement
  def place(line, column)
    colorize(self, "[#{line};#{column}", mode='f')
  end
  
  def save_pos;    colorize(self, "[", mode='s');  end
  def return_pos;  colorize(self, "[", mode='u');  end
  
  def colorize(text, color_code, mode='m')
    "#{color_code}#{mode}#{text}[0m"
  end
  
end

module RPkg

  class Logging
    
    def initialize
      @PROMPTS  = {
        :info   => '>>'.bold.green,
        :warn   => '**'.bold.yellow,
        :error  => '!!'.red,
        :fatal  => '!!'.bold.red,
        :debug  => '**'.bold.cyan,
        :prompt => '>>'.bold.green
      }
      
      @LOG = File.open('/tmp/rpkg.log', 'a+')

      @last_action = nil
    end
    
    def method_missing(method, *args, &block)
      if method =~ /^(info|warn|error|fatal|debug|prompt)$/
        out = "#{@PROMPTS[method.to_sym]} #{args.join ' '}"
        @LOG.write out
        if method =~ /^prompt$/
          print "\r#{' '*($rpkg[:columns] - 1)}\r#{out}"
        else
          puts "\r#{' '*($rpkg[:columns] - 1)}\r#{out}"
        end
      else
        super.method_missing(method, *args, &block)
      end
    end
    
  end

  class UI < Logging
    def reset
      @last_action = nil
    end
    
    def progressBar(size=10, args={})
      if @last_action == nil or (Time.now.to_f - @last_action) >= 0.5
        if [Fixnum, Integer, Float].include? size.class and args.class == Hash
          if not args.keys.include? :percent or not args.keys.include? :message
            raise ArgumentError, 'You must provide a percent value.'
          else
            
            # Beginning and basic setup
            out             = "%2s" % '>>'.bold.green
            out_r           = '>>'
            outEnd          = nil
            outEnd_r        = nil

            # Do we have an end bit? (size/count)
            if args.keys.include? :total and args.keys.include? :current
              if args.keys.include? :size and args[:size] == true
                hashes      = (size * (args[:percent].to_f / 100.0)).to_i
                outEnd      = "(%s / %s)" % [formatSize(args[:current].to_i),
                                             formatSize(args[:total].to_i)]
              else
                outEnd      = "(%s / %s)" % [args[:current].to_i,
                                             args[:total].to_i]
              end
            end
            # However if speed is also specified, we overwrite the size/count bit
            if args.keys.include? :speed and args[:speed] == true
              if (Time.now.to_i - args[:dl_start]) >= 1
                outEnd       = "(%s/s)" % formatSize(args[:current] / (Time.now.to_i - args[:dl_start]))
              end
            end

            # just in case we already have an end bit (size/count)
            if outEnd != nil
              tmp           = outEnd
              hashes        = (size * (args[:percent].to_f / 100.0)).to_i
              outEnd        = "%s [%s%s] (%3d%%)" % [tmp,
                                                     '#'.bold.black * hashes,
                                                     '-'.bold.black * (size - hashes),
                                                     args[:percent].to_i]
              outEnd_r      = "%s [%s%s] (%3d%%)" % [tmp,
                                                     '#' * hashes,
                                                     '-' * (size - hashes),
                                                     args[:percent].to_i]
            else
              hashes        = (size * (args[:percent].to_f / 100.0)).to_i
              outEnd        = "[%s%s] (%3d%%)" % ['#'.bold.black * hashes,
                                                  '-'.bold.black * (size - hashes),
                                                  args[:percent].to_i]
              outEnd_r      = "[%s%s] (%3d%%)" % ['#' * hashes,
                                                  '-' * (size - hashes),
                                                  args[:percent].to_i]
            end

            
            # Dynamically size our message bit
            usedlen         = ((out_r.length + outEnd_r.length) + 2)
            availen         = ($rpkg[:columns] - usedlen)
            
            # now lets normalize our line
            while (usedlen + availen) > $rpkg[:columns]
              availen -= 1
            end
            while (usedlen + availen) < $rpkg[:columns]
              availen += 1
            end

            if args[:message].length >= availen
              msglen        = "%s" % args[:message][0..(availen - 1)]
            else
              msglen        = "%s%s" % [args[:message], 
                                        ' '*(availen - args[:message].length)]
            end

            # And format our final line
            msgOut          = "\r%s %s %s" % [out,
                                              msglen,
                                              outEnd]
            print msgOut
            @last_action = Time.now.to_f
          end
        end
      end
    end
    
    def formatSize(bytes=nil)
      sizes = {
        :gbyte => ((1024*1024)*1024),
        :mbyte => (1024*1024),
        :kbyte => 1024
      }
      if bytes <= sizes[:kbyte]
        return "#{bytes}b"
      elsif bytes > sizes[:kbyte] and bytes <= sizes[:mbyte]
        return "%.02fKb" % (bytes.to_f / sizes[:kbyte].to_f)
      elsif bytes > sizes[:mbyte] and bytes <= sizes[:gbyte]
        return "%.02fMb" % (bytes.to_f / sizes[:mbyte].to_f)
      else
        return "%.02fGb" % (bytes.to_f / sizes[:gbyte].to_f)
      end
    end
    
    def formatTime(secs=nil)
      allowed_types = [Fixnum, Integer, Float]
      if secs and allowed_types.include?(secs.class)
        times = {
          :minute => 60,
          :hour   => (60*60),
          :day    => ((60*60)*24),
          :week   => (((60*60)*24)*7),
        }
        retv = {
          :seconds => nil,  :minutes => nil,
          :hours   => nil,  :days    => nil,
          :weeks   => nil
        }
        # Weeks
        if secs >= times[:week]
          retv[:weeks]   = (secs / times[:week])
          secs           = (secs % times[:week])
        end
        # Days
        if secs >= times[:day]
          retv[:days]    = (secs / times[:day])
          secs           = (secs % times[:day])
        end
        # Hours
        if secs >= times[:hour]
          retv[:hours]   = (secs / times[:hour])
          secs           = (secs % times[:hour])
        end
        # Minutes
        if secs >= times[:minute]
          retv[:minutes] = (secs / times[:minute])
          retv[:seconds] = (secs % times[:minute])
        end
        s = Array.new
        retv.keys.reverse.each {|k|
          if retv[k]
            s.push "#{retv[k]}#{k[0]}"
          end
        }
        return s.join
      end
    end
    
  end

end  
