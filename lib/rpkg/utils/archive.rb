################################################################################
#
# Copyright 2013 Mike 'Fuzzy'  Partin   <fuzzy@fu-manchu.org>
# Copyright 2013 Matt 'Roider' Schiros  <schirs@invisihosting.com>
#
################################################################################
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  
################################################################################

##-----------------
## Module imports

# Stdlib
#---------

# 3rd party
#------------
require 'libarchive'

# Internal
#-----------
require 'rpkg/ui'

##--------------
## Main logics
module RPkg

  module Utils

    def self.extractArchive(archive=nil, dest=$rpkg[:dirs][:temp])
      counter   = 0
      totsize   = 0
      cursize   = 0
      processed = 0
      stats     = {:dirs => 0, :files => 0, :link => 0}
      if File.exist?(archive)
        $UI.prompt("Examining #{File.basename(archive)} (this may take a few seconds.)")
        Archive.read_open_filename(archive) {|ar|
          while entry = ar.next_header
            counter += 1
            if entry.directory?
              stats[:dirs] += 1
            elsif entry.symbolic_link?
              stats[:link] += 1
            elsif entry.regular?
              totsize += ar.read_data.size
              stats[:files] += 1
            end
          end
        }
        Archive.read_open_filename(archive) {|ar|
          processed = 0
          while entry = ar.next_header
            if entry.directory?
              if not File.directory?("#{$rpkg[:dirs][:temp]}/#{entry.pathname}")
                Dir.mkdir("#{$rpkg[:dirs][:temp]}/#{entry.pathname}")
                processed += 1
              else
                processed += 1
              end
            elsif entry.regular?
              if not File.exist?("#{$rpkg[:dirs][:temp]}/#{entry.pathname}")
                data = ar.read_data
                out  = File.open("#{$rpkg[:dirs][:temp]}/#{entry.pathname}", "w+")
                out.write(data)
                out.close
                processed += 1
                cursize   += data.size
              else
                processed += 1
              end
            end
            $UI.progressBar(20, {
                              :percent => ((processed.to_f / counter.to_f) * 100.0),
                              :message => "Extracting #{File.basename(archive)}",
                              :current => cursize.to_i,
                              :total   => totsize.to_i,
                              :size    => true
                            })
#            print "\r%s Extracting %-36s %s" % ['>>'.bold.green,
#                                                File.basename(archive),
#                                                RPkg::UI.progressBar((processed.to_f / counter.to_f) * 100.0, 20)]
          end
        }
        sleep 1
        $UI.progressBar(20, {
                          :percent => 100,
                          :message => "Extracting #{File.basename(archive)}",
                          :current => cursize.to_i,
                          :total   => totsize.to_i,
                          :size    => true
                        })
      end
    end

  end

end
