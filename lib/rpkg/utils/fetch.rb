################################################################################
#
# Copyright 2013 Mike 'Fuzzy'  Partin   <fuzzy@fu-manchu.org>
# Copyright 2013 Matt 'Roider' Schiros  <schirs@invisihosting.com>
#
################################################################################
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  
################################################################################

##-----------------
## Module imports

# Stdlib
#---------
require 'open-uri'

# 3rd party
#------------

# Internal
#-----------
require 'rpkg/ui'

module RPkg

  module Utils

    def self.fetchUri(uri=nil, outfile=nil)
      if outfile
        fp = File.open(outfile, 'w+')
      else
        fp = File.open("#{$rpkg[:dirs][:base]}/tmp/#{File.basename(uri)}", 'w+')
      end
      size_t  = nil
      current = 0
      tstamp = Time.now.to_i
      open(uri,
           :content_length_proc => lambda {|t|
             if t && 0 < t
               size_t = t
             end
           },
           :progress_proc => lambda {|size_c|
             progress = ((size_c.to_f / size_t.to_f) * 100.to_f)
             current  = size_c
             $UI.progressBar(20, {
                               :percent  => progress,
                               :message  => "Fetching #{File.basename(uri)}",
                               :current  => current.to_i,
                               :total    => size_t.to_i,
                               :size     => true,
                               :speed    => true,
                               :dl_start => tstamp
                             })
           }) {|f| f.each_line {|buf| fp.write(buf) } }
      fp.close
      sleep 1
      $UI.progressBar(20, {
                        :percent  => 100,
                        :message  => "Fetching #{File.basename(uri)}",
                        :current  => current.to_i,
                        :total    => size_t.to_i,
                        :speed    => true,
                        :size     => true,
                        :dl_start => tstamp
                      })
      puts
    end

    def self.fetchAndVerify(uri=nil, hash=nil)
      if uri and hash
        if uri.class == String and [Hash, String].include?(hash.class)
          # lets create some convenience variables that will make everything
          # not only a bit nicer to read, but also to write.
          size_t   = nil # total size
          tmpdir   = "#{$rpkg[:dirs][:base]}/tmp"
          filename = File.basename(uri)
          download = "#{tmpdir}/#{filename}"

          # ok, now lets get down to buisness
          # if we have a temp directory...
          if not File.directory? tmpdir
            Dir.mkdir(tmpdir)
          end
          # Do we also already have the file downloaded?
          if File.exist? "#{download}"
            # Is it a valid download though?
            if hash.class == String
              md5 = Digest::MD5.file("#{download}").hexdigest
              if md5 == hash
                print "%s %s\n" % ['>>'.bold.green, 
                                   "#{filename} download verified."]
              else
                puts "%s ERROR downloading #{filename}" % '!!'.bold.red
                puts "%s Currently downloaded file is damaged." % '!!'.bold.red
                puts "%s Recorded hash: %s" % ["**".bold.yellow, hash]
                puts "%s Actual hash:   %s" % ["**".bold.yellow, md5]
                puts "%s Refetching..." % ">>".bold.green
                File.delete("#{download}")
                fetchUri(uri)
              end
            else
              raise ArgumentError, 'String of an MD5 hash only please.'
            end
          else
            fetchUri(uri)
          end
          md5 = Digest::MD5.file("#{download}").hexdigest
          if hash != md5
            puts "%s The download is unverified." % "!!".bold.red
            puts "%s Check the package manifest and reverify your uris" % "!!".bold.red
          end
        else
        end
        
      end
      
    end
    
  end
  
end
