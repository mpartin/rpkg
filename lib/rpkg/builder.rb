################################################################################
#
# Copyright 2013 Mike 'Fuzzy' Partin <fuzzy@fu-manchu.org>
#
################################################################################
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  
################################################################################

###################
## Module imports

## Stdlib

## 3rd party

## Internal
require 'rpkg/utils'

module RPkg
  
  class Dsl
    
    def initialize(file)
      @LOG                   = RPkg::Logging.new
      @DEBUG                 = true
      @Pkg                   = Hash.new
      @Pkg[:depends]         = Array.new
      @Pkg[:uris]            = Array.new
      @Pkg[:package]         = nil
      @Pkg[:version]         = nil
      @Pkg[:patchlevel]      = nil
      @Pkg[:patches]         = Array.new
      @Pkg[:configure]       = Hash.new
      @Pkg[:configure][:arg] = nil
      @Pkg[:configure][:cmd] = nil
      @Pkg[:configure][:env] = nil
      @Pkg[:build]           = Hash.new
      @Pkg[:build][:arg]     = nil
      @Pkg[:build][:cmd]     = nil
      @Pkg[:build][:env]     = nil
      @Pkg[:install]         = Hash.new
      @Pkg[:install][:arg]   = nil
      @Pkg[:install][:cmd]   = nil
      @Pkg[:install][:env]   = nil
      
      instance_eval File.read(file)
      process
    end
    
    #################
    ## Misc options
    
    def depends(*args)
      # TODO:
      # argument verification
      args.each do |arg|
        s = arg.class.to_s
        if arg.class != String
          raise(
                ArgumentError,
                "Invalid argument (#{s}), depends only accepts strings."
                )
        else
          @Pkg[:depends].push arg
          @LOG.debug "added dependancy: #{arg.strip}."
        end
      end
      # option setting
    end
    
    def package(*args)
      #puts "#{self.class.to_s}##{__method__.to_s} #{args.inspect}"
      if not args[0]
        raise ArgumentError, 'You must provide at minimum a pkgname argument'
      else
        @Pkg[:package]    = args[0] if args[0]
        @Pkg[:version]    = args[1] if args[1]
        @Pkg[:patchlevel] = args[2] if args[2]
      end
      @LOG.info "Installing #{@Pkg[:package]}-#{@Pkg[:version]}"
    end
    
    def uri(uri=nil, md5=nil)
      # TODO:
      # argument validation
      if [uri, md5].include? nil
        if md5.class != String or not [String, Array].include? uri.class
          raise ArgumentError, "#{super.to_s}."
        end
      end
      # option setting
      if uri.class == Array
        uri.each {|url|
          @Pkg[:uris].push [url, md5]
          if @DEBUG
            @LOG.debug "uri #{url} added"
          end
        }
      else
        @Pkg[:uris].push [uri, md5]
        if @DEBUG
          @LOG.debug "uri #{uri} added"
        end
      end
    end
    
    def patch(arg)
      @Pkg[:patches].push arg
      @LOG.debug "Added patch: #{File.basename(arg)}"
    end
    
    ##################
    ## Configure set
    def configure_args(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end
    
    def configure_env(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end
    
    def configure_cmd(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end
    
    ######################
    ## Compile/Build set
    def build_args(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end
    
    def build_env(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end
    
    def build_cmd(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end
    
    ################
    ## Install set
    def install_args(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end
    
    def install_env(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end
    
    def install_cmd(*args)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => args.inspect
      }
    end		
    
    ####################
    ## Package actions
    
    def command(cmd=nil)
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => cmd.inspect
      }
    end
    
    def configure
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => 'nil'
      }
    end
    
    def build
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => 'nil'
      }
    end
    
    def install
      debug args={
        :class  => self.class.to_s,
        :method => __method__.to_s,
        :args   => 'nil'
      }
    end
    
    private
    def process
      # Fetch
      @Pkg[:uris].each {|item|
        RPkg::Utils.fetchAndVerify(item[0], item[1])
      }
      # Extract
      @Pkg[:uris].each {|item|
        RPkg::Utils.extractArchive("#{$rpkg[:dirs][:temp]}/#{File.basename(item[0])}")
        puts
      }
      # Configure
      # Build
      # Install
    end
    
  end
  
end

