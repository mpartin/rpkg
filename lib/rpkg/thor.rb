################################################################################
#
# Copyright 2013 Mike 'Fuzzy' Partin <fuzzy@fu-manchu.org>
#
################################################################################
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  
################################################################################

###################
## Module imports

## Stdlib

## 3rd party
require 'thor'

## Internal
require 'rpkg/ui'

module RPkg
  
  # All thor related logic for handling command line options and commands
  module CommandLine
    
    class Processor < Thor

      default_task :default_action

      desc '', ''
      def default_action
        puts <<-EOM

rpkg v0.0.1
Usage: rpkg [command] <args> <...>

EOM
        help
      end
      
      desc "bootstrap",
      "Bootstrap the builder environment (database)"
      def bootstrap
        # TODO:
        # Create/Open the db file
        # Create/Validate tables
      end
      
      desc "search [TOKEN]", 
      "Search through the package definitions for [TOKEN]"
      def search(token)
        if File.exist? $config[:PD_DIR]
          Dir.glob("#{$config[:PD_DIR]}/*").each {|entry|
            Dir.glob("#{entry}/*").each {|sentry|
              puts "#{sentry}"
            }
          }
        end
      end
      
      desc 'install [TOKEN]',
      'Build package described by [category/package]'
      def install(token)
        #puts "#{$PROMPT} Installing#{$COLON} #{File.basename(token)}"
        RPkg::Dsl.new do
          begin
            eval(File.open(token).read)
          rescue ArgumentError => m
            e = "#{'ERROR'.bold.red}#{':'.bold.underline.white}"
            b = "#{m.backtrace.inspect.cyan}#{':'.bold.underline.white}"
            puts "#{e} #{b} #{m}"
            exit
          end
        end
      end
      
      desc 'remove [TOKEN]',
      'Remove package described by [category/package]'
      def remove(token)
      end
      
      desc 'info [TOKEN]',
      'Show package info for [category/package]'
      def info(token)
      end
      
      desc 'update',
      'Update package definitions'
      def update
      end

      desc 'use <global> [TOKEN]',
      'Use (globally) the installed package represented by TOKEN'
      def use(scope='local', token=nil)
      end

      desc 'drop <global> [TOKEN]',
      ''
      def drop(scope='local', token=nil)
      end

      desc 'show [TOKEN]',
      ''
      def show(token=nil)
      end

      desc 'list [TOKEN]',
      'List all packages in [category]'
      def list(token)
        # TODO
        # argument verification
        # action
        if File.exists? "#{$config[:PD_DIR]}/#{token}"
          Dir.glob("#{$config[:PD_DIR]}/#{token}/*").each {|itm|
            puts "#{$PROMPT} #{token}/#{File.basename(itm)}"
          }
        end
      end
      
    end  
    
  end
  
end
